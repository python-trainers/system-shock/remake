from trainerbase.main import run

from gui import run_menu
from injections import update_player_coord_pointer
from scripts import script_engine


def on_initialized():
    script_engine.start()
    update_player_coord_pointer.inject()


def on_shutdown():
    script_engine.stop()


if __name__ == "__main__":
    run(run_menu, on_initialized, on_shutdown)

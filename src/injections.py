from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import pm

from memory import player_coords_pointer, player_hp_pointer_pointer


# "SystemReShock-Win64-Shipping.exe" + 0x32270EE
# AOB: 48 03 ?? 8B 44 ?? ?? 2B 44
infinite_ammo = AllocatingCodeInjection(
    pm.base_address + 0x32270EE,
    """
        add rdi, rax
        mov eax, [rsp + 0x38]

        cmp rax, 0
        je skip

        cmp rax, 1
        je skip

        cmp rax, 0xA5
        je skip

        test r12, r12
        je skip

        cmp byte [r12 + 0x70], 1
        jne skip

        add eax, [rsp + 0x40]

        skip:

        sub eax, [rsp + 0x40]
        mov [rbx + 0x20], rdi
    """,
    original_code_length=15,
)

# AOB: 4C 8B A6 D8 00 00 00 48
one_hit_kill = AllocatingCodeInjection(
    pm.base_address + 0x353CCB5,
    f"""
        push rax
        mov rax, {player_hp_pointer_pointer}
        mov rax, [rax]

        test rax, rax
        je skip

        sub rax, 0xD8
        cmp rax, rsi
        je skip

        mov dword [rsi + 0xD8], 1

        skip:

        pop rax

        mov r12, [rsi + 0xD8]
        lea rdx, [rbp - 0x19]
        mov r15, [rsi + 0xE0]
    """,
    original_code_length=18,
)

# AOB: FF 48 85 C0 74 26 0F 10 88 D0 01 00 00
update_player_coord_pointer = AllocatingCodeInjection(
    pm.base_address + 0x35B1AD5,
    f"""
        push rbx

        mov rbx, {player_coords_pointer}
        mov [rbx], rax

        pop rbx

        movups xmm1, [rax + 0x1D0]
        movaps xmm0, xmm1
        movss [rsp + 0x60], xmm1
    """,
    original_code_length=16,
)

from dearpygui import dearpygui as dpg
from trainerbase.gui import (
    CodeInjectionUI,
    GameObjectUI,
    SeparatorUI,
    SpeedHackUI,
    TeleportUI,
    add_components,
    simple_trainerbase_menu,
)

from injections import infinite_ammo, one_hit_kill
from objects import energy, hp
from teleport import tp


@simple_trainerbase_menu("System Shock Remake", 800, 300)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                GameObjectUI(hp, "HP", "Shift + F1", "F1", 500),
                GameObjectUI(energy, "Energy", "Shift + F2", "F2", 500),
                CodeInjectionUI(infinite_ammo, "Infinite Ammo", "F3"),
                CodeInjectionUI(one_hit_kill, "One Hit Kill", "F4"),
                SeparatorUI(),
                SpeedHackUI(),
            )
        with dpg.tab(label="Teleport"):
            add_components(TeleportUI(tp))

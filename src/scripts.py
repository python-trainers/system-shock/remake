from contextlib import suppress

from pymem.exception import MemoryReadError, MemoryWriteError
from trainerbase.scriptengine import ScriptEngine, enabled_by_default

from objects import hp, player_hp_pointer


script_engine = ScriptEngine()


@enabled_by_default
@script_engine.simple_script
def update_player_hp_pointer():
    try:
        actual_hp_pointer = hp.address.resolve()
    except MemoryReadError:
        actual_hp_pointer = 0

    with suppress(MemoryWriteError):
        player_hp_pointer.value = actual_hp_pointer

from trainerbase.gameobject import GameFloat, GameInt, GameUnsignedLongLong
from trainerbase.memory import Address, pm

from memory import player_coords_pointer, player_hp_pointer_pointer


player_status_address = Address(pm.base_address + 0x5153970, [0xE8, 0x230, 0x20, 0x38])

hp = GameInt(player_status_address + [0x20, 0xD8])
energy = GameInt(player_status_address + [0x68, 0xD8])
player_hp_pointer = GameUnsignedLongLong(player_hp_pointer_pointer)

player_coords_address = Address(player_coords_pointer, [0x1D0])

player_x = GameFloat(player_coords_address)
player_y = GameFloat(player_coords_address + 0x4)
player_z = GameFloat(player_coords_address + 0x8)
